﻿namespace UmaibowConversion
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.priceBox = new System.Windows.Forms.TextBox();
            this.umaiButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tirolButton = new System.Windows.Forms.Button();
            this.umaiLabel = new System.Windows.Forms.Label();
            this.tirolLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.hourlyPayButton = new System.Windows.Forms.Button();
            this.hourlyPayLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.blackThunderLabel = new System.Windows.Forms.Label();
            this.blackThunderButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(44, 28);
            this.priceBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(132, 22);
            this.priceBox.TabIndex = 1;
            // 
            // umaiButton
            // 
            this.umaiButton.Location = new System.Drawing.Point(44, 71);
            this.umaiButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.umaiButton.Name = "umaiButton";
            this.umaiButton.Size = new System.Drawing.Size(203, 29);
            this.umaiButton.TabIndex = 2;
            this.umaiButton.Text = "うまい棒で換算";
            this.umaiButton.UseVisualStyleBackColor = true;
            this.umaiButton.Click += new System.EventHandler(this.CalcUmaibowConversion);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "円";
            // 
            // tirolButton
            // 
            this.tirolButton.Location = new System.Drawing.Point(44, 109);
            this.tirolButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tirolButton.Name = "tirolButton";
            this.tirolButton.Size = new System.Drawing.Size(203, 29);
            this.tirolButton.TabIndex = 3;
            this.tirolButton.Text = "チロルチョコで換算";
            this.tirolButton.UseVisualStyleBackColor = true;
            this.tirolButton.Click += new System.EventHandler(this.CalcTirolConversion);
            // 
            // umaiLabel
            // 
            this.umaiLabel.Location = new System.Drawing.Point(265, 78);
            this.umaiLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.umaiLabel.Name = "umaiLabel";
            this.umaiLabel.Size = new System.Drawing.Size(75, 15);
            this.umaiLabel.TabIndex = 5;
            this.umaiLabel.Text = "?????";
            this.umaiLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tirolLabel
            // 
            this.tirolLabel.Location = new System.Drawing.Point(265, 114);
            this.tirolLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tirolLabel.Name = "tirolLabel";
            this.tirolLabel.Size = new System.Drawing.Size(75, 15);
            this.tirolLabel.TabIndex = 6;
            this.tirolLabel.Text = "?????";
            this.tirolLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(348, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "本";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(348, 114);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "個";
            // 
            // hourlyPayButton
            // 
            this.hourlyPayButton.Location = new System.Drawing.Point(44, 184);
            this.hourlyPayButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.hourlyPayButton.Name = "hourlyPayButton";
            this.hourlyPayButton.Size = new System.Drawing.Size(203, 29);
            this.hourlyPayButton.TabIndex = 5;
            this.hourlyPayButton.Text = "時給で換算";
            this.hourlyPayButton.UseVisualStyleBackColor = true;
            this.hourlyPayButton.Click += new System.EventHandler(this.CalcHourlyPayConversion);
            // 
            // hourlyPayLabel
            // 
            this.hourlyPayLabel.Location = new System.Drawing.Point(265, 192);
            this.hourlyPayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hourlyPayLabel.Name = "hourlyPayLabel";
            this.hourlyPayLabel.Size = new System.Drawing.Size(75, 15);
            this.hourlyPayLabel.TabIndex = 10;
            this.hourlyPayLabel.Text = "?????";
            this.hourlyPayLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(348, 192);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "時間　（大阪府最低時給936円で計算）";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(348, 150);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "個";
            // 
            // blackThunderLabel
            // 
            this.blackThunderLabel.Location = new System.Drawing.Point(265, 150);
            this.blackThunderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.blackThunderLabel.Name = "blackThunderLabel";
            this.blackThunderLabel.Size = new System.Drawing.Size(75, 15);
            this.blackThunderLabel.TabIndex = 13;
            this.blackThunderLabel.Text = "?????";
            this.blackThunderLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // blackThunderButton
            // 
            this.blackThunderButton.Location = new System.Drawing.Point(44, 146);
            this.blackThunderButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.blackThunderButton.Name = "blackThunderButton";
            this.blackThunderButton.Size = new System.Drawing.Size(203, 29);
            this.blackThunderButton.TabIndex = 4;
            this.blackThunderButton.Text = "ブラックサンダーで換算";
            this.blackThunderButton.UseVisualStyleBackColor = true;
            this.blackThunderButton.Click += new System.EventHandler(this.CalcBlackThunderConversion);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 242);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.blackThunderLabel);
            this.Controls.Add(this.blackThunderButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hourlyPayLabel);
            this.Controls.Add(this.hourlyPayButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tirolLabel);
            this.Controls.Add(this.umaiLabel);
            this.Controls.Add(this.tirolButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.umaiButton);
            this.Controls.Add(this.priceBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "うまい棒換算";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.Button umaiButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button tirolButton;
        private System.Windows.Forms.Label umaiLabel;
        private System.Windows.Forms.Label tirolLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button hourlyPayButton;
        private System.Windows.Forms.Label hourlyPayLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label blackThunderLabel;
        private System.Windows.Forms.Button blackThunderButton;
    }
}

