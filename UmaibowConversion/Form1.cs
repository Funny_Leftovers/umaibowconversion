﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UmaibowConversion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CalcUmaibowConversion(object sender, EventArgs e)
        {
            int price;
            bool success = int.TryParse(this.priceBox.Text, out price);

            if (success)
            {
                int umaiNum = price / 10;
                this.umaiLabel.Text = umaiNum.ToString();
            }
        }

        private void CalcTirolConversion(object sender, EventArgs e)
        {
            int price;
            bool success = int.TryParse(this.priceBox.Text, out price);

            if (success)
            {
                int tirolNum = price / 20;
                this.tirolLabel.Text = tirolNum.ToString();
            }
        }

        private void CalcHourlyPayConversion(object sender, EventArgs e)
        {
            int price;
            bool success = int.TryParse(this.priceBox.Text, out price);

            if (success)
            {
                float hourlyPay = price / 936.0f;
                this.hourlyPayLabel.Text = hourlyPay.ToString("F2");
            }
        }

        private void CalcBlackThunderConversion(object sender, EventArgs e)
        {
            int price;
            bool success = int.TryParse(this.priceBox.Text, out price);

            if (success)
            {
                int blackThunderNum = price / 30;
                this.blackThunderLabel.Text = blackThunderNum.ToString();
            }
        }
    }
}
